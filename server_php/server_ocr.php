<?php

require_once("lib/nusoap.php");

$ns = "http://www.devxtend.com/webservice/";

$server = new soap_server();
$server ->configureWSDL('comision',$ns);
$server ->wsdl ->schemaTargetNamespace = $ns;
$server ->register('calculo',array('monto' => 'xsd:string'),array('return' => 'xsd:string'),$ns);
$server ->register('calculoMax',array('monto' => 'xsd:string'),array('return' => 'xsd:string'),$ns);

function calculo ($monto){
	$comision = $monto * 0.15;
	return new soapval('return','xsd:string',$comision);
}

function calculoMax ($monto){
	$comision = $monto * 0.30;
	return new soapval('return','xsd:string',$comision);
}


$server ->service($HTTP_RAW_POST_DATA);

?>