#!/usr/bin/python

import SOAPpy
import socket,os
import base64
import time
from SOAPpy import WSDL
import string


img = "b.png"

url="http://invasor.ath.cx/server_php/server_ocr.php?wsdl"
server = WSDL.Proxy(url)

print 'Available methods:'
for method in server.methods.keys() :
  print method
  ci = server.methods[method]
  # you can also use ci.inparams
  for param in ci.outparams :
     # list of the function and type 
     # depending of the wsdl...
     print param.name.ljust(20) , param.type

jpgtxt = base64.encodestring(open(img,"rb").read())
print "Ocr Resultante "
print server.sendImage(bytes(jpgtxt))

raw_input()
