#!/usr/bin/python

#import SOAPpy
from SOAPpy import WSDL
import string
namespace = "http://src/"
#url = "http://localhost/~juan/wsdl/HelloService.wsdl"
url = "http://invasor.ath.cx/~juan/wsdl/HelloService.wsdl"
#url="http://invasor.ath.cx/~juan/wsdl/NewWebService.wsdl"
#url = "http://invasor.ath.cx:8081"
#server = SOAPpy.SOAPProxy("http://localhost/~juan/wsdl/HelloService.wsdl")
#server = SOAPpy.SOAPProxy("http://localhost/~juan/wsdl/HelloService.wsdl")
#server = SOAPpy.SOAPProxy(url)
server = WSDL.Proxy(url)
#server.config.dumpSOAPOut = 1            
#server.config.dumpSOAPIn = 1
print 'Available methods:'
for method in server.methods.keys() :
  print method
  ci = server.methods[method]
  # you can also use ci.inparams
  for param in ci.outparams :
   # list of the function and type 
   # depending of the wsdl...
    print param.name.ljust(20) , param.type
  print
print server.pelo("algo")

