#!/usr/bin/python

import SOAPpy
import SocketServer
import threading
import time
import base64
import os
import cherrypy
import sys
import shutil

global Id;
Id= 0;
global esperando;
esperando=0

#creo mi TCP Handler
class MiTcpHandler(SocketServer.BaseRequestHandler):    
    #sobrescribo la funcion handle
    def handle(self):
        data= ""
        global Id
        print "Id en elservidor ", Id
        #------archivo conf con cherry-----------
        base_dir = os.getcwd()
        conf_file = os.path.join(base_dir, 'demo_server.conf')
        if not os.path.isfile(conf_file):
            conf_file = os.path.join(base_dir, 'demo_tupac.conf')
        cherrypy.log("app base_dir: %s" % base_dir,context='SETUP', traceback=False)
        cherrypy.config.update(conf_file)
        print "cherry"
        actual = cherrypy.config['actual']
        Id_actual = actual + "/" + str(Id)
        if not os.path.isdir(Id_actual):
            os.mkdir(Id_actual)
        print "actual" , Id_actual
        ocr  = cherrypy.config['ocr']
        print "ocr"
        ejecutar = cherrypy.config['ejecutar']
        print "ejecutar"  
        while data != "salir":
            #intento recibir informacion
            try:                            
                data= self.request.recv(5000000)
                print "Recivir"
                #------Crear Archivo e Imagen----
                f = open(Id_actual+"/"+"jpg1_b64.txt", "w")
                f.write(data)
                f.close()
                newjpgtxt = open(Id_actual+"/"+"jpg1_b64.txt","rb").read()
                g = open(Id_actual+"/"+"out.png", "wb")              
                g.write(base64.decodestring(newjpgtxt))
                g.close()
                #------MD5 y renombrar imagen----             
                os.chdir(Id_actual)               
                os.system("md5sums -u "+ g.name +" > md5.txt")                
                print "MD5"
                codigo = open("md5.txt", "rb")
                print "MD51"
                linea = codigo.readline()
                print "MD52"
                word = linea.split(" ")
                print "MD53"
                while 1:
                    if os.path.isfile("out.png"):
                        break
                if os.path.isfile(word[0]+".png"):
                    os.remove(word[0]+".png")                    
                shutil.copy("out.png", word[0]+".png")
                print "MD54"
                codigo.close()
                os.remove("md5.txt")
                os.remove("out.png")
                #------Ejecuatar Ocr ---------
                ruta= os.chdir(ocr)                
                archivo = os.system(ejecutar + Id_actual+"/"+ "salida.txt")                                
                ruta= os.chdir(actual)                   
                s= open(Id_actual+"/"+"salida.txt", "rb").read()
                self.request.send(s)
                print "Rescponder"              
                time.sleep(1) #espero 0.1 segundos antes de leer neuvamente          
            #si hubo un error lo digo y termino el handle
            except:
                print "El cliente D/C o hubo un error"
                data = "salir"
                

#no se assusten Creo una clase llamada ThreadServer
class ThreadServer (SocketServer.ThreadingMixIn, SocketServer.ForkingTCPServer):
    pass

#----Conectarse----
def hello():
  return "Bienvenido al Servidor"
#----Identificarse-----
def login(u,p):
  if u=="tupac" and p=="123":
    return True
  elif u=="juan" and p=="456":
      return True
  else:
      return False
#----ObtenerSocket----
def getSocket():
  return 9998
#----EnviarImagen-----
def sendImage():
    #host & port
    global esperando
    if esperando==0:
        host ="localhost"
        port= 9998
        #creo el server
        server = ThreadServer((host,port),MiTcpHandler)
        #creo un thread del server
        server_thread = threading.Thread(target=server.serve_forever)
        #empiezo el thread
        server_thread.start()    
        print "EsperandiImagen..."
        esperando = 1
        
#-------ObtenerId------
def getId(u):
    if u=="juan":
        global Id
        Id = 1
        return 1
    elif u=="tupac":
        global Id
        Id = 2
        return 2



server = SOAPpy.SOAPServer(("localhost", 8081))

server.registerFunction(hello)

server.registerFunction(login)

server.registerFunction(getId)

server.registerFunction(getSocket)

server.registerFunction(sendImage)

print "Servidor Corriendo"

server.serve_forever()


