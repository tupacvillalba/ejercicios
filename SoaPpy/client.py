#!/usr/bin/python

import SOAPpy
import socket,os
import base64
import time

#-------EnviarImagenSocket-----------
def sendImageClient(p):
    msj =""
    host, port = "localhost" , p
    #creo un socket y me conecto
    sock= socket.socket()
    sock.connect((host,port))
    img="b.png"
    while msj != "salir":
        msj = raw_input(">Presione una tecla para enviar imagen o salir para cancelar: ")
        #intento mandar msj
        if msj != "salir":                
            try:
               jpgtxt = base64.encodestring(open(img,"rb").read())
               #f = open("jpg1_b64.txt", "w")
               #f.write(jpgtxt)
               #f.close()
               sock.send(jpgtxt)        
               while 1:
                   data = sock.recv(1024)
                   if data:                  
                       print "data ", data
                       break               
            # si no se puede entonces salgo
            except:
                print "> !!!! No se envia la imagen !!!!"                
    sock.close() #recuerden cerrar el socket

#-------PPAL--------------
server = SOAPpy.SOAPProxy("http://localhost:8081/")

try:
    #--------Conect-----------
    print server.hello()

    #---------Login-----------
    print "> ---LOGIN---"
    entro=False
    while(not entro):
        usuario=raw_input("> Usuario: ")
        password=raw_input("> Password: ")    
        if server.login(usuario,password):
            print "> Exito!"
            entro=True
            Id=server.getId(usuario)
        else:
            print "> Fracaso!"

    #---------EnviarImagen--------
    server.sendImage()
    sendImageClient(server.getSocket())
            
    raw_input("> Esperando...")
except:
    print "> No se pudo conectar servidor o hubo un error"
