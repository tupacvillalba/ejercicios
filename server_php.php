<?php

require_once("lib/nusoap.php");

$ns = "http://invasor.ath.cx/server_php/";

$server = new soap_server();
$server ->configureWSDL('comision',$ns);
$server ->wsdl ->schemaTargetNamespace = $ns;
$server ->register('calculo',array('monto' => 'xsd:string'),array('return' => 'xsd:string'),$ns);
$server ->register('calculoMax',array('monto' => 'xsd:string'),array('return' => 'xsd:string'),$ns);

$server ->register('sendImage',array('img' => 'xsd:byte'),array('return' =>
 'xsd:string'),$ns);

function sendImage ($img){
 	//$g = fopen('out.png', 'w')
	//fwrite($g, base64_decode($img))
	chmod ("out.png", 0777);    
	$g = fopen("out.png", "w+");
	//chmod ("salida.xml", 0777);
	//$f = fopen("salida.xml", "w+");
	fwrite ($g, base64_decode($img));
	exec("/home/ubuntu/dev/ocr/build/acr/./test_api_py /home/ubuntu/dev/ocr/source/MaterialesCliente/xml/config/config.xml /var/www/server_php/out.png /var/www/server_php/salida.xml");
	chmod("salida.xml", 0777);	
	//$s = fopen("/var/www/server_php/salida.xml","r");
	//fclose($s);
	//$s = readfile("salida.xml");
	$f = file_get_contents("salida.xml");
	return new soapval('return','xsd:string',$f);
}

function calculo ($monto){
	$comision = $monto * 0.15;
	return new soapval('return','xsd:string',$comision);
}

function calculoMax ($monto){
	$comision = $monto * 0.30;
	return new soapval('return','xsd:string',$comision);
}


$server ->service($HTTP_RAW_POST_DATA);

?>
