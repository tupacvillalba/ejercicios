<?
require_once('lib/nusoap.php'); 

//Instanciamos un nuevo objeto almacenado en $server, de la Clase nusoap
$server = new soap_server(); 

//Definimos $ns, como la variable que contiene el PATH del KIT
$ns="http://127.0.0.1/webservices/";

//Al objeto instanciado en $server, le aplicamos la funci�n configureWSDL(), de la clase nusoap.php, para este ejemplo el nombre del Web Service ser� CAYTICS EC
$server->configureWSDL('CAYTICS EC',$ns);

//Indicamos el Namespace del Server
$server->wsdl->schemaTargetNamespace=$ns;

//La funci�n que realizar� la suma, se llama suma(), y esta definida l�neas m�s abajo. A continuaci�n de acuerdo a los par�metros que reciba la funci�n //suma() definimos la entrega de valores, en la sintaxis del Web Service.
$server->register('suma',array('a' => 'xsd:string','b' => 'xsd:string'),array('return' => 'xsd:string'),$ns);

//Definimos la funci�n suma($a,$b)
function suma($a, $b)
{	
    	$resultado = $a + $b;
    	return new soapval('return','xsd:string',$resultado);
}

//Hacemos especie de (Try to) al invocar el servicio
if (isset($HTTP_RAW_POST_DATA))
{
    	$input = $HTTP_RAW_POST_DATA;
}
else
{
    	$input = implode("\r\n", file('php://input'));
}
$server->service($input);
exit;

?>