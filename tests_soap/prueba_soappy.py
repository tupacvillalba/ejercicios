#!/usr/bin/python

#from SOAPpy import SOAPProxy
from SOAPpy import WSDL

#url = 'http://www.webservicex.net/WeatherForecast.asmx'
url = 'http://graphical.weather.gov/xml/SOAP_server/ndfdXMLserver.php'
server = WSDL.Proxy(url);
#print server.GetWeatherByPlaceName('Dallas');
print 'Available methods:'
for method in server.methods.keys() :
  print method
  ci = server.methods[method]
  # you can also use ci.inparams
  for param in ci.outparams :
     # list of the function and type 
     # depending of the wsdl...
     print param.name.ljust(20) , param.type

print "##################"
print server.LatLonListZipCode('33128');
